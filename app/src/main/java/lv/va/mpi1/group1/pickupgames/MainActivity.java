package lv.va.mpi1.group1.pickupgames;



import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.Bundle;

import java.util.Arrays;
import java.util.ArrayList;

import lv.va.mpi1.group1.pickupgames.ui.main.MainFragment;

public class MainActivity extends AppCompatActivity

{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_activity);

        SharedPreferences mSharedPreferences = getSharedPreferences(DataAccess.sharedPrefFile, MODE_PRIVATE);

        if(!mSharedPreferences.contains(DataAccess.gameLength)) {
            // Shared preferences empty, let's set it up from resources
            Resources res = getResources();
            DataAccess dataAccess = new DataAccess(mSharedPreferences);
            dataAccess.saveGameLength(res.getInteger(R.integer.default_game_length));
            ArrayList<String> teamList = new ArrayList(Arrays.asList(res.getStringArray(R.array.default_team_names)));
            dataAccess.saveTeams(teamList);
            dataAccess.saveTeamNumber(res.getInteger(R.integer.default_team_count));
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MainFragment.newInstance())
                    .commitNow();
        }
    }
}
