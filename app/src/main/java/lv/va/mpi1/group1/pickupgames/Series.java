package lv.va.mpi1.group1.pickupgames;

import java.util.ArrayList;
import java.util.HashMap;

import lv.va.mpi1.group1.pickupgames.Match;

public class Series {
    public static final int MODE_ROUND_ROBIN = 0;
    public static final int MODE_KING_OF_THE_COURT = 1;
    private HashMap<String, ArrayList<Match>> teamMatches;;
    private ArrayList<Match> matches = new ArrayList<>();
    private ArrayList<String> teams;
    private int mode;

    public Series(ArrayList<String> teams, int mode) {
        teamMatches =  new HashMap<>();
        this.teams = teams;
        this.mode = mode;
        for (String team : teams) {
            teamMatches.put(team, new ArrayList<Match>());
        }
    }

    public void addMatchPlayed(String teamOne, String teamTwo, int resultTeamOne, int resultTeamTwo) {
        Match match = new Match(teamOne, teamTwo, resultTeamOne, resultTeamTwo);
        addMatchPlayed(match);
    }
    public void addMatchPlayed(Match match) {
        match.matchNumber = matches.size()+1;
        matches.add(match);
        teamMatches.get(match.teamOne).add(match);
        teamMatches.get(match.teamTwo).add(match);

    }
    private Match getLastMatch() {
        if (matches.size() > 0) {
            return matches.get(matches.size() - 1);
        } else {
            return null;
        }
    }
    private String getTeamWithLessMatchesPlayed() {
        return getTeamWithLessMatchesPlayed(this.teams);
    }
    private String getTeamWithLessMatchesPlayed(ArrayList<String> teamList) {

        int lessPlayedMatches = teamMatches.get(teamList.get(0)).size();
        ArrayList<String> lessPlayedTeams = new ArrayList<>();
        lessPlayedTeams.add(teamList.get(0));

        for (String team : teamList.subList(1, teamList.size())) {
            int teamPlayedMatchesCount = teamMatches.get(team).size();
            if (teamPlayedMatchesCount < lessPlayedMatches) {
                lessPlayedMatches = teamPlayedMatchesCount;
                lessPlayedTeams = new ArrayList<>();
                lessPlayedTeams.add(team);
            }
            if (teamPlayedMatchesCount == lessPlayedMatches) {
                lessPlayedTeams.add(team);
            }
        }
        return lessPlayedTeams.get(0);
    }
    private String getTeamWithLessMatchesPlayed(String excludeTeam) {
        ArrayList<String> excludedTeamList = new ArrayList<>();
        for (String team: teams) {
            if (!team.equals(excludeTeam)) {
                excludedTeamList.add(team);
            }
        }
        return getTeamWithLessMatchesPlayed(excludedTeamList);
    }
    public Match getProposedNextMatchTeams() {
        String teamOne;
        if (this.mode == MODE_KING_OF_THE_COURT) {
            if (matches.size()>0) {
                teamOne = getLastMatch().getWinner();
            } else {
                teamOne = teams.get(0);
            }
        } else {
            teamOne = getTeamWithLessMatchesPlayed();
        }
        String teamTwo = getTeamWithLessMatchesPlayed(teamOne);
        return new Match(teamOne, teamTwo);
    }
    public Match getProposedNextMatchOponent(String teamOne) {
        String teamTwo = getTeamWithLessMatchesPlayed(teamOne);
        return new Match(teamOne, teamTwo);
    }
    public int getMode() {
        return mode;
    }

}
