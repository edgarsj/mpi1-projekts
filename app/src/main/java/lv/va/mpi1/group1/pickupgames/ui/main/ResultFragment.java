package lv.va.mpi1.group1.pickupgames.ui.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

import com.google.android.material.textfield.TextInputLayout;

import lv.va.mpi1.group1.pickupgames.Match;
import lv.va.mpi1.group1.pickupgames.R;
import lv.va.mpi1.group1.pickupgames.Series;


public class ResultFragment extends Fragment {

    private MainViewModel mViewModel;

    private Spinner mTeamOneSpinner;
    private Spinner mTeamTwoSpinner;

    private TextInputLayout mTeamOneResult;
    private TextInputLayout mTeamTwoResult;

    private Button mButtonNextGame;
    private Button mButtonEndSeries;

    private Series mCurrentSeries;
    private Match mCurrentMatch;
    private Match mNextMatch;

    private static String TAG = "ResultFragment";

    public ResultFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.result_fragment, container, false);
        mViewModel = ViewModelProviders.of(this.getActivity()).get(MainViewModel.class);
        mCurrentMatch = mViewModel.getCurrentMatch().getValue();
        mCurrentSeries = mViewModel.getCurrentSeries().getValue();
        mCurrentSeries.addMatchPlayed(mCurrentMatch);
        if (mCurrentSeries.getMode() == Series.MODE_KING_OF_THE_COURT) {
            mNextMatch = mCurrentSeries.getProposedNextMatchOponent(mCurrentMatch.teamOne); // winner not known yet
        } else {
            mNextMatch = mCurrentSeries.getProposedNextMatchTeams();
        }

        mTeamOneResult = rootView.findViewById(R.id.team_one_result);
        mTeamOneResult.setHint(mCurrentMatch.teamOne);
        mTeamTwoResult = rootView.findViewById(R.id.team_two_result);
        mTeamTwoResult.setHint(mCurrentMatch.teamTwo);

        String[] teamArray = mViewModel.getTeams().getValue().toArray(new String[0]);

        mTeamOneSpinner = rootView.findViewById(R.id.team1_spinner);
        ArrayAdapter<String> teamOneAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, teamArray);
        teamOneAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTeamOneSpinner.setAdapter(teamOneAdapter);
        mTeamOneSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                checkSelectedTeams();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });


        mTeamTwoSpinner = rootView.findViewById(R.id.team2_spinner);
        ArrayAdapter<String> teamTwoAdapter = new ArrayAdapter(getContext(), android.R.layout.simple_spinner_item, teamArray);
        teamTwoAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mTeamTwoSpinner.setAdapter(teamTwoAdapter);
        mTeamTwoSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                checkSelectedTeams();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });
        setSpinnerValue(mTeamOneSpinner, mNextMatch.teamOne);
        setSpinnerValue(mTeamTwoSpinner, mNextMatch.teamTwo);



        mTeamOneResult.getEditText().addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                processResult();
            }
        });
        mTeamTwoResult.getEditText().addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable s) {}

            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                processResult();
            }
        });


        mButtonNextGame = rootView.findViewById(R.id.buttonStartNext);
        mButtonNextGame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                nextGame();
            }
        });
        mButtonNextGame.setEnabled(false);

        mButtonEndSeries = rootView.findViewById(R.id.buttonEndSeries);
        mButtonEndSeries.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                endSeries();
            }
        });
        mButtonEndSeries.setEnabled(false);

        return rootView;
    }

    private void setSpinnerValue(Spinner spinner, String value) {
        int position = ((ArrayAdapter<String>)spinner.getAdapter()).getPosition(value);
        spinner.setSelection(position);
    }

    private boolean checkTeamScores() {
        try {
            int teamOneScore = Integer.parseInt(mTeamOneResult.getEditText().getText().toString());
            int teamTwoScore = Integer.parseInt(mTeamOneResult.getEditText().getText().toString());
            mCurrentMatch.teamOneScore = teamOneScore;
            mCurrentMatch.teamTwoScore = teamTwoScore;
            Log.d(TAG, "valid team scores");
            return true;
        } catch (NumberFormatException nfe) {
            Log.d(TAG, "invalid team scores");
            return false;
        }
    }

    private void checkSelectedTeams() {
        boolean validTeamScores = checkTeamScores();
        String teamOneSelected = (String) mTeamOneSpinner.getSelectedItem();
        String teamTwoSelected = (String) mTeamTwoSpinner.getSelectedItem();
        if (teamOneSelected.equals(teamTwoSelected)) {
            mButtonNextGame.setEnabled(false);
            mButtonEndSeries.setEnabled(false);
        } else {
            mNextMatch.teamOne = teamOneSelected;
            mNextMatch.teamTwo = teamTwoSelected;
            if (validTeamScores) {
                mButtonNextGame.setEnabled(true);
                mButtonEndSeries.setEnabled(true);
            }
        }
    }

    private void processResult() {
        boolean valid = checkTeamScores();
        if (valid)
        {
            mNextMatch = mCurrentSeries.getProposedNextMatchTeams();
            setSpinnerValue(mTeamOneSpinner, mNextMatch.teamOne);
            setSpinnerValue(mTeamTwoSpinner, mNextMatch.teamTwo);
            mButtonNextGame.setEnabled(true);
            mButtonEndSeries.setEnabled(true);
        } else {
            mButtonNextGame.setEnabled(false);
            mButtonEndSeries.setEnabled(false);
        }


    }

    private void nextGame() {
        mViewModel.setCurrentMatch(mNextMatch);
        GameFragment newGameFragment = new GameFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, newGameFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void endSeries() {
        MainFragment mainFragment = new MainFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, mainFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
