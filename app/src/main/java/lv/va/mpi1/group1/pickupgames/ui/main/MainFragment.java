package lv.va.mpi1.group1.pickupgames.ui.main;

import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.ArrayAdapter;

import java.util.ArrayList;

import lv.va.mpi1.group1.pickupgames.DataAccess;
import lv.va.mpi1.group1.pickupgames.R;
import lv.va.mpi1.group1.pickupgames.Series;
import lv.va.mpi1.group1.pickupgames.TeamListAdapter;

public class MainFragment extends Fragment {

    private MainViewModel mViewModel;
    private FloatingActionButton mFabAdd;
    private NumberPicker mGameLength;
    private View mView;
    private Spinner mSpinner;

    private String TAG = "MainFragment";
    private static final int MIN_GAME_LENGTH = 1;
    private static final int MAX_GAME_LENGTH = 10;

    private DataAccess mdataAccess;

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        SharedPreferences sharedPreferences = this.getActivity().getSharedPreferences(DataAccess.sharedPrefFile, Context.MODE_PRIVATE);
        mViewModel = ViewModelProviders.of(this.getActivity()).get(MainViewModel.class);
        mdataAccess = new DataAccess(sharedPreferences);
        int teamNumber = mdataAccess.getTeamNumber();

        View rootView = inflater.inflate(R.layout.main_fragment, container, false);
        this.mView = rootView;
        RecyclerView recyclerView = rootView.findViewById(R.id.teamlist);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        ArrayList<String> teamNames = mdataAccess.getTeamNames(teamNumber);
        mViewModel.setTeams(teamNames);
        mViewModel.setGameLength(mdataAccess.getGameLength());
        mViewModel.setGameMode(mdataAccess.getGameMode());
        final TeamListAdapter teamListAdapter = new TeamListAdapter(teamNames);
        recyclerView.setAdapter(teamListAdapter);
        ItemTouchHelper itemTouchHelper = new
                ItemTouchHelper(new SwipeToDeleteCallback(teamListAdapter));
        itemTouchHelper.attachToRecyclerView(recyclerView);

        mGameLength = rootView.findViewById(R.id.number_picker_game_length);
        mGameLength.setMinValue(MIN_GAME_LENGTH);
        mGameLength.setMaxValue(MAX_GAME_LENGTH);
        mGameLength.setValue(mdataAccess.getGameLength());
        mGameLength.setWrapSelectorWheel(false);
        mGameLength.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
                public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                    Log.d(TAG, String.format("new picker value: %d", newVal) );
                    mViewModel.setGameLength(newVal);
                }
            });

        mFabAdd = rootView.findViewById(R.id.addTeamBtn);
        mFabAdd.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                // Trying to get a preset team name for next position;
                ArrayList teamNames = mViewModel.getTeams().getValue();
                String newTeamName = mdataAccess.getTeamName(teamNames.size());
                teamNames.add(newTeamName);
                teamListAdapter.notifyItemInserted(teamNames.size());
            }
        });

        mSpinner = rootView.findViewById(R.id.mode_spinner);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.mode_list, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(adapter);
        mSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                mViewModel.setGameMode(position);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }
        });



        Button startSeries = rootView.findViewById(R.id.buttonStart);
        startSeries.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startSeries();
            }
        });

        return rootView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }
    private void startSeries() {
        ArrayList<String> teams = mViewModel.getTeams().getValue();
        int mode = mViewModel.getGameMode();
        mdataAccess.saveGameMode(mode);
        mdataAccess.saveTeamNumber(teams.size());
        mdataAccess.saveTeams(teams);
        int gameLength = mViewModel.getGameLength();
        mdataAccess.saveGameLength(gameLength);
        Series currentSeries = new Series(teams, mode);
        mViewModel.setCurrentSeries(currentSeries);
        mViewModel.setCurrentMatch(currentSeries.getProposedNextMatchTeams());
        GameFragment newGameFragment = new GameFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, newGameFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }
    public class SwipeToDeleteCallback extends ItemTouchHelper.SimpleCallback {
        private TeamListAdapter mAdapter;
        public SwipeToDeleteCallback(TeamListAdapter adapter) {
            super(0,ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT);
            mAdapter = adapter;
        }
        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            int position = viewHolder.getAdapterPosition();
            mAdapter.deleteItem(position);
        }
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder fromView, RecyclerView.ViewHolder toView) {
            // no op
            return false;
        }
    }

}
