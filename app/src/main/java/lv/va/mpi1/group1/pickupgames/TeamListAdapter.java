package lv.va.mpi1.group1.pickupgames;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import android.view.LayoutInflater;

import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class TeamListAdapter extends RecyclerView.Adapter<TeamListAdapter.MyViewHolder> {
    private ArrayList<String> teamNames;

    public TeamListAdapter(ArrayList<String> teamNames) {
        this.teamNames = teamNames;
    }

//    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_team_input, parent, false);
        return new MyViewHolder(itemView, new EditTextListener());
    }
    class MyViewHolder extends RecyclerView.ViewHolder {
        EditTextListener editTextListener;
        EditText editTeamName;
        MyViewHolder(View view, EditTextListener editTextListener) {
            super(view);
            this.editTeamName = view.findViewById(R.id.editTeamName);
            this.editTextListener = editTextListener;
            this.editTeamName.addTextChangedListener(editTextListener);

        }
    }
    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.editTextListener.updatePosition(holder.getAdapterPosition());
        String name = teamNames.get(position);
        holder.editTeamName.setText(name);
    }
    @Override
    public int getItemCount() {
        return teamNames.size();
    }
    public void addItem(String name) {
        teamNames.add(name);
        this.notifyItemInserted(teamNames.size());
    }
    private class EditTextListener implements TextWatcher {
        private int position;

        public void updatePosition(int position) {
            this.position = position;
        }
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            // no op
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            teamNames.set(position, charSequence.toString());
            Log.d("TeamNames", String.format("Changed position %d to %s", position, charSequence.toString()));
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // no op
        }
    }
    public void deleteItem(int position) {
//        mRecentlyDeletedItem = mListItems.get(position);
//        mRecentlyDeletedItemPosition = position;
        teamNames.remove(position);
        notifyItemRemoved(position);
//        showUndoSnackbar();
    }
}
