package lv.va.mpi1.group1.pickupgames;

import android.content.SharedPreferences;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
//import android.os.Bundle;


public class DataAccess {
    private static final String TAG = "PickupGamesDataAccess";

    public static final String sharedPrefFile = "lv.va.mpi1.group1.pickupgamesprefs";
    public static final String gameLength = "gameLength";
    public static final String teamNumber = "teamNumber";
    public static final String teamNames = "teamNames";
    public static final String teamName = "teamName";
    public static final String gameMode = "gameMode";


    private SharedPreferences sharedPreferences;

    public DataAccess(SharedPreferences sharedPrefs)
    {
        sharedPreferences = sharedPrefs;
    }

    public int getGameLength() {
        return sharedPreferences.getInt(gameLength, 5);
    }
    public void saveGameLength(int gameLength) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(DataAccess.gameLength, gameLength);
        editor.commit();
    }

    public ArrayList<String> getTeamNames(int teamNumber) {
        Log.d(TAG, String.format("getTeamNames with teamNumber = %d", teamNumber));
        ArrayList<String> teamList = new ArrayList<>(teamNumber);
        for(int i=0; i<teamNumber; i++)
            teamList.add(sharedPreferences.getString(teamName+ "_" + i, null));
        return teamList;
    }
    public void saveTeams(ArrayList<String> teamList) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        int teamNumber = teamList.size();
        for (int counter = 0; counter < teamNumber; counter++) {
            editor.putString(teamName + "_" + counter, teamList.get(counter));
        }
        editor.putInt(DataAccess.teamNumber, teamNumber);
        editor.commit();
    }
    public int getTeamNumber() {
        return sharedPreferences.getInt(teamNumber, 3);
    }

    public void saveTeamNumber(int teamNumber) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(DataAccess.teamNumber, teamNumber);
        editor.commit();
    }
    public int getGameMode() {
        return sharedPreferences.getInt(gameMode, Series.MODE_ROUND_ROBIN);
    }
    public void saveGameMode(int gameMode) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(DataAccess.gameMode, gameMode);
        editor.commit();
    }

    public String getTeamName(int position) {
        return sharedPreferences.getString(teamName + "_" + position, "");
    }


}
