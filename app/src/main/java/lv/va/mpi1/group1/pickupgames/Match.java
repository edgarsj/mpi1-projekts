package lv.va.mpi1.group1.pickupgames;

public class Match {
    public int matchNumber;
    public String teamOne;
    public String teamTwo;
    public int teamOneScore;
    public int teamTwoScore;
    public Match(String teamOne, String teamTwo, int teamOneScore, int teamTwoScore) {
        this.teamOne = teamOne;
        this.teamTwo = teamTwo;
        this.teamOneScore = teamOneScore;
        this.teamTwoScore = teamTwoScore;
    }
    public Match(String teamOne, String teamTwo) {
        this.teamOne = teamOne;
        this.teamTwo = teamTwo;
    }
    public String getWinner() {

        if (teamOneScore > teamTwoScore) {
            return this.teamOne;
        } else {
            return this.teamTwo;
        }

    };

}
