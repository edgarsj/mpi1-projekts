package lv.va.mpi1.group1.pickupgames.ui.main;

import java.util.ArrayList;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import lv.va.mpi1.group1.pickupgames.Match;
import lv.va.mpi1.group1.pickupgames.Series;

public class MainViewModel extends ViewModel {

    private MutableLiveData<ArrayList<String>> teams = new MutableLiveData<>();
    private MutableLiveData<Integer> gameLength = new MutableLiveData<>();
    private MutableLiveData<Integer> gameMode = new MutableLiveData<>();
    private MutableLiveData<Series> currentSeries = new MutableLiveData<>();
    private MutableLiveData<Match> currentMatch = new MutableLiveData<>();

    public MutableLiveData<ArrayList<String>> getTeams() {
        if (teams == null) {
            teams= new MutableLiveData<>();
            loadTeams();
        }
        return teams;
    }
    private void loadTeams() {
        ArrayList<String> teamList = new ArrayList<>();
        teams.setValue(teamList);
    }
    public void setTeams(ArrayList<String> teamList) {
        teams.setValue(teamList);
    }
    public int getGameLength() {
        return gameLength.getValue().intValue();
    }
    public void setGameLength(int gameLength) {
        this.gameLength.setValue(gameLength);
    }
    public void setCurrentSeries(Series series) {
        this.currentSeries.setValue(series);
    }
    public MutableLiveData<Series> getCurrentSeries() {
        return currentSeries;
    }
    public void setCurrentMatch(Match match) {
        this.currentMatch.setValue(match);
    }
    public MutableLiveData<Match> getCurrentMatch() {
        return currentMatch;
    }
    public int getGameMode() {
        return gameMode.getValue();
    }
    public void setGameMode(int mode) {
        this.gameMode.setValue(mode);
    }


}


