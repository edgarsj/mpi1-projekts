package lv.va.mpi1.group1.pickupgames.ui.main;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.ViewModelProviders;

import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.TextView;
import android.widget.Toast;

import lv.va.mpi1.group1.pickupgames.Match;
import lv.va.mpi1.group1.pickupgames.R;

public class GameFragment extends Fragment {

    private MainViewModel mViewModel;
    private Chronometer mGameTimer;
    private Button mBtnStartPause, mBtnGameOver;
    private int gameLengthMinutes;
    private boolean isTimerRunning = false;
    private long timerPausedAt;
    private long timerElapsedMillis;

    public GameFragment() {
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.game_fragment, container, false);
        mViewModel = ViewModelProviders.of(this.getActivity()).get(MainViewModel.class);
        gameLengthMinutes = mViewModel.getGameLength();
        final Match currentMatch = mViewModel.getCurrentMatch().getValue();

        mBtnStartPause = rootView.findViewById(R.id.buttonStartPause);
        mBtnGameOver = rootView.findViewById(R.id.buttonGameOver);


        mGameTimer = rootView.findViewById(R.id.gameTimer);
        mGameTimer.setBase(SystemClock.elapsedRealtime() + 1000*60*gameLengthMinutes);
        mGameTimer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener(){
            @Override
            public void onChronometerTick(Chronometer timer) {
                if (SystemClock.elapsedRealtime() > timer.getBase()) {
                   // timer end reached
                    timer.stop();
                    mBtnStartPause.setEnabled(false);
                    isTimerRunning = false;
                    timerElapsedMillis = 60*1000*gameLengthMinutes;
                    Toast notify_game_over = Toast.makeText(getContext(), R.string.game_finished_notification, Toast.LENGTH_SHORT);
                    notify_game_over.show();
                    // TODO: add airhorn sound

                }

            }
        });

        timerPausedAt = SystemClock.elapsedRealtime();
        timerElapsedMillis = 0;

        mBtnStartPause.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(isTimerRunning) {
                    mGameTimer.stop();
                    timerPausedAt = SystemClock.elapsedRealtime();
                    timerElapsedMillis =  SystemClock.elapsedRealtime() - (mGameTimer.getBase() - 60*1000*gameLengthMinutes) ;
                    Log.d("gameFragment",String.format("Timer elapsed: %d", timerElapsedMillis));
                    isTimerRunning = false;
                    mBtnStartPause.setText(R.string.resume);
                } else {
                    mGameTimer.setBase(mGameTimer.getBase() + (SystemClock.elapsedRealtime()- timerPausedAt));
                    mGameTimer.start();
                    isTimerRunning = true;
                    mBtnStartPause.setText(R.string.pause);
                }
            }
        });

        mBtnGameOver.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                    mGameTimer.stop();
                    isTimerRunning = false;
                    timerPausedAt = SystemClock.elapsedRealtime();
                    timerElapsedMillis =  SystemClock.elapsedRealtime() - (mGameTimer.getBase() - 60*1000*gameLengthMinutes) ;
                    Log.d("gameFragment",String.format("Game over for teams: %s vs %s with %d seconds elapsed", currentMatch.teamOne, currentMatch.teamTwo, timerElapsedMillis/1000));
                    gameOver();
            }
        });

        TextView currentlyPlaying = rootView.findViewById(R.id.currently_playing);

        currentlyPlaying.setText(String.format("%s %s %s %s",
                getResources().getString(R.string.playing), currentMatch.teamOne,
                getResources().getString(R.string.versus), currentMatch.teamTwo ));

        return rootView;
    }

    private void gameOver() {
        ResultFragment newResultFragment = new ResultFragment();
        FragmentTransaction fragmentTransaction = getActivity().getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.container, newResultFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

}
